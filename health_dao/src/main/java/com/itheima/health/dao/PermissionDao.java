package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Permission;

public interface PermissionDao {
    //新增权限数据
    void add(Permission permission);

    //分页查询
    Page<Permission> findByCondition(String queryString);

    //查询要删除的权限是否被角色使用了
    int findCountByPermissionId(int id);

    //删除权限
    void deleteById(int id);

    //通过id查询权限
    Permission findById(int id);

    //编辑权限
    void update(Permission permission);
}
