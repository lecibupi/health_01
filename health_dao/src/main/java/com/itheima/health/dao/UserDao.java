package com.itheima.health.dao;

import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author 冯晓东
 * @date 2020/7/26  18:16
 */
public interface UserDao {
    User findByUsername(String username);

    List<Menu> getMenuByUsername(String username);

    List<User> findPage(String queryString);

    List<Role> UsertableData();

    User findById(Integer id);

    List<Integer> UsercheckitemIds(Integer id);

    void UserRoleRelationAdd(@Param("userId") Integer id,@Param("roleId") Integer roleId);

    void UserAdd(User formData);

    void UserRoleRelationDel(@Param("userId") Integer id);

    void UserEdit(User formData);

    void Userdelete(Integer id);

     <!--修改密码-->
    <update id="setPassword" parameterType="string">
    update t_user set `password`=#{newpassword} where username=#{username};
    </update>

}
