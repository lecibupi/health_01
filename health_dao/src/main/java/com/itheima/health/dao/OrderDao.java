package com.itheima.health.dao;

import com.itheima.health.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderDao {
    //添加套餐
    void add(Order order);
    //根据条件查询套餐
    List<Order> findByCondition(Order order);
    //根据套餐id查询预约检查成功后的信息
    Map<String,Object> findById(Integer id);
    //根据日期查询对应的预约数
    Integer findOrderCountByDate(String date);

    Integer findOrderCountAfterDate(String date);
    Integer findVisitsCountByDate(String date);

    Integer findVisitsCountAfterDate(String date);

    List<Map> findHotSetmeal();

    //查询某个时间点之间的预约数
    Integer findOrderCountBetweenDate(@Param("startDay") String startDay, @Param("endDay") String endDay);
}
