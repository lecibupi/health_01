package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckItem;

import java.util.List;

public interface CheckItemDao {

    //查询所有的检查项
    List<CheckItem> findAll();

    //新增检查项
    void add(CheckItem checkItem);

    //分页查询检查项
    Page<CheckItem> pageFindAll(String queryString);

    //回显数据
    CheckItem findById(Integer id);
//编辑检查项
    void editDao(CheckItem checkItem);
 //删除前先查看
    Integer find(int id);
 //删除
    void delete(int id);
}
