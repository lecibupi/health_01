package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MemberDao {
    List<Member> findAll();
    Page<Member> selectByCondition(String queryString);
    void add(Member member);//插入会员
    void deleteById(Integer id);
    Member findById(Integer id);
    Member findByTelephone(String telephone);
    void edit(Member member);
    //查询过去一年注册的会员，分成一个月一个月的查询
    Integer findMemberCountBeforeDate(String date);
    Integer findMemberCountByDate(String date);
    Integer findMemberCountAfterDate(String date);
    Integer findMemberTotalCount();

      //套餐饼图，查询订单表中套餐出现的次数
      List<Map<String,Object>> findOrderByMemberCount();
    //会员男女饼状图，获取会员男女数量
    List<Map<String, Object>> getMemberReportGender();
    //会员年龄段占比饼状图，获取各个年龄段会员数量
    Map<String, Integer> getMemberReportAge();

    List<Map<String,Object>> getMonthDate(@Param("beginTimeStr") String beginTimeStr, @Param("endTimeStr")String endTimeStr);

    List<Map<String,Object>> getYearData(@Param("beginfirstDayOfWeek") String beginfirstDayOfWeek, @Param("endfirstDayOfWeek")String endfirstDayOfWeek);
}
