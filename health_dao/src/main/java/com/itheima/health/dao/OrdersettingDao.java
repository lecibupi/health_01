package com.itheima.health.dao;

import com.itheima.health.pojo.OrderSetting;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrdersettingDao {
    //查询当前日期对应的Ordersetting
    OrderSetting findByOrderDate(Date orderDate);
    //修改最大预约数
    void update(OrderSetting orderSetting);
     //当查询到的Ordersetting为空时就添加这个Ordersetting
    void addOrderSetting(OrderSetting orderSetting);
       //显示所有的预约信息
    List<Map<String, Integer>> findAllOrder(@Param("startDate") String startDate,@Param("endDate") String endDate);
    //更新当前日期的已预约人数
    void updateNumber(OrderSetting orderSetting);



    //预约设置清除
    void deleteSetting(String date);




}
