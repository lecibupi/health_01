package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Menu;

public interface MenuDao {
    //新增菜单
    void add(Menu menu);

    //分页查询
    Page<Menu> findByCondition(String queryString);

    // 查询要删除的检查项是否被检查组使用了
    int findCountByMenuId(int id);

    //删除菜单
    void deleteById(int id);

    //通过ID查询菜单
    Menu findById(int id);

    //编辑菜单
    void update(Menu menu);

    //
    int findpath(String path);

    Integer findFuid(String substring);
}
