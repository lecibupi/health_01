package com.itheima.health.dao;

import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao {
    List<Role> findPage(String queryString);

    List<Permission> RolePermissionRelation();

    List<Menu> RoleMenuRelation();

    void RoleAdd(Role formData);

    void RolePermissionRelationAdd(@Param("roleId") Integer roleId,@Param("permissionId") Integer permissionId);

    void RoleMenuRelationAdd(@Param("roleId")Integer id,@Param("menuId") Integer menuId);

    Role FindByRoleId(Integer id);

    List<Permission> RolePermissionRelationAdver(Integer id);
//    List<Integer> RolePermissionRelationAdver(Integer id);

    List<Menu> RoleMenuRelationAdver(Integer id);
//    List<Integer> RoleMenuRelationAdver(Integer id);

    void RolePermissionRelationDel(Integer id);

    void RoleMenuRelationDel(Integer id);

    void RoleDel(Integer id);

    void RoleEdit(Role formData);

    List<Integer> RoleMenuMaxId();
}
