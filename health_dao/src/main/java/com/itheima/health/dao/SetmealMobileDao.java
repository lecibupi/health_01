package com.itheima.health.dao;

import com.itheima.health.pojo.Setmeal;

import java.util.List;

public interface SetmealMobileDao {
    //查询所有的套餐
    List<Setmeal> findAllSermeal();
    //根据套餐id查询对应的套餐信息方式一：
    Setmeal findById(int id);

    //根据套餐id查询对应的套餐信息方式二：
    Setmeal findById1(int id);
}
