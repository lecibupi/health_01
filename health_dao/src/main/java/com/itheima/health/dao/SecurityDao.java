package com.itheima.health.dao;

import com.itheima.health.pojo.User;

public interface SecurityDao {
    //权限管理
    User findByUsername(String username);
}
