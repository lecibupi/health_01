package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SetmealDao {





    //添加套餐
    void add(Setmeal setmeal);
    //添加检查组与套餐的关系
    void addSetmealAndChenkgroup(@Param("setmealid") Integer setmealid,@Param("checkgroupId") Integer checkgroupId);

    //分页条件查询
    Page<Setmeal> findPage(String queryString);

    //查询检查组与套餐的关系
    Integer find(int id);

    //先删除关系表
    void deleteSetmealCheckgroup(int setmearlId);

    //删除套餐
    void delete(int id);

    //回显数据==套餐信息
    Setmeal lookdata(int setmealId);

    List<Integer> findAllCheckgroudId(int setmearlId);
       //添加套餐与检查组的关系
    void edit(Setmeal setmeal);
    //清理七牛云上没有被数据库使用的照片
    //查询数据库里面的所有图片名
    List<String> findImgs();



}

