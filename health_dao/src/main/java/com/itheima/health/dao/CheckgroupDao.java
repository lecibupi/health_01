package com.itheima.health.dao;


import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CheckgroupDao {
    //分页条件查询
    Page<CheckGroup> findPage(String queryString);

    //添加检查组的信息
    void add(CheckGroup checkGroup);

    //添加检查组与检查项的关系信息
     /*由于有两个相同的参数需要对其去别名
     * */
    void addItemsAndGroup(@Param("checkGroupId") Integer checkGroupId, @Param("checkitemId") Integer checkitemId);

    //根据id查询是否与检查项有关
    Integer findByid(int id);
//先将检查组与检查项的关系删除
    void deleteCheckGroupCheckItem(int setmearlId);

    //删除检查组
    void delete(int id);

    //回显数据
    CheckGroup lookdata(int id);
    //回显选中的检查项
    List<Integer> checkitemIds(Integer checkitemId);

    //修改检查组
    void update(CheckGroup checkGroup);
    /*查询所有的检查项在新增套餐时使用*/
    List<CheckGroup> findAll();
}
