package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    //登录时根据用户的电话查询这个是否为会员
    @Override
    public Member findMember(String telephone) {
        return  memberDao.findByTelephone(telephone);

    }
    //登录时添加会员
    @Override
    public void add(Member member) {
       memberDao.add(member);
    }

    //查询每月注册的会员数
    @Override
    public List<Integer> findReportMember(List<String> months) {
        List<Integer> memberCountAfterDates=null;
        if(months != null) {
          memberCountAfterDates = new ArrayList<Integer>();
            for (String month : months) {

                Integer memberCountAfterDate = memberDao.findMemberCountBeforeDate(month + "-31");
                memberCountAfterDates.add(memberCountAfterDate);
            }
        }
        return memberCountAfterDates;
    }

    //套餐饼图 查询套餐在订单表里的数量
    @Override
    public List<Map<String,Object>> findsetmealCount() {
        return   memberDao.findOrderByMemberCount();

    }
    //会员男女饼状图，获取会员男女数量
    @Override
    public List<Map<String, Object>> getMemberReportGender() {
        return memberDao.getMemberReportGender();
    }
    //会员年龄段占比饼状图，获取各个年龄段会员数量
    @Override
    public Map<String, Integer> getMemberReportAge() {
        return memberDao.getMemberReportAge();
    }
}
