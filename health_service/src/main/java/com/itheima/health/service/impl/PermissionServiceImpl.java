package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.PermissionDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Permission;
import com.itheima.health.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionDao permissionDao;

    //新增权限数据查询
    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    //分页查询
    @Override
    public PageResult<Permission> findPage(QueryPageBean queryPageBean) {
        // 第一步
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        // 第二步, 紧接着的dao查询语句会被分页
        // 条件查询
        if(!StringUtils.isEmpty(queryPageBean.getQueryString())){
            // queryString是否有值，有值 模糊查询，拼接%
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString()+ "%");
        }
        Page<Permission> page = permissionDao.findByCondition(queryPageBean.getQueryString());
        // 线程变量同步技术 ThreadLocal
        // page.getTotal() 总数
        // page.getResult() 分页的结果集
        PageResult<Permission> pageResult = new PageResult<>(page.getTotal(), page.getResult());
        return pageResult;
    }

    //删除权限
    @Override
    public void deleteById(int id) {
        //查询要删除的权限是否被角色使用了
        int count = permissionDao.findCountByPermissionId(id);
        if (count > 0){
            throw new HealthException("改权限已经被使用了,不能删除");
        }
        //如果没有被使用
        permissionDao.deleteById(id);
    }

    //通过id查询权限
    @Override
    public Permission findById(int id) {
        return permissionDao.findById(id);
    }

    //编辑权限
    @Override
    public void update(Permission permission) {
        permissionDao.update(permission);
    }
}
