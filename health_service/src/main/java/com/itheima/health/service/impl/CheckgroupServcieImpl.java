package com.itheima.health.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.CheckgroupDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckgroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service(interfaceClass = CheckgroupService.class)
public class CheckgroupServcieImpl implements CheckgroupService {

    @Autowired
    private CheckgroupDao checkgroupDao;

    /*新增检查组信息
    1.增加检查组的信息需要将检查组的信息存入t_checkgroup表 ，同时获取插入的id
    2.还有要将检查组对应的检查项的信息写入t_checkgroup_checkitem表
    * */
    @Override
    @Transactional
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //添加检查组的信息
        checkgroupDao.add(checkGroup);
        //添加检查组与检查项的关系
        Integer checkGroupId = checkGroup.getId();
        //判断检查项的id是否存在，存在添加
        if (null != checkitemIds) {
            // 添加检查组与检查项的关系
            for (Integer checkitemId : checkitemIds) {
                checkgroupDao.addItemsAndGroup(checkGroupId,checkitemId);
            }
        }
    }

    @Override
    public PageResult<CheckGroup> findPage(QueryPageBean queryPageBean) {

        /*检查组分页查询插件
         *
         * */

        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        if (!StringUtils.isEmpty(queryPageBean.getQueryString())) {
            //有条件就需要拼接
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString() + "%");
        }

        //调用dao返回page
        Page<CheckGroup> page = checkgroupDao.findPage(queryPageBean.getQueryString());
        PageResult<CheckGroup> sss = new PageResult<>(page.getTotal(), page.getResult());
        return sss;
    }

    /*删除检查组
       1.先确定是否与检查项有关系 有关联就不能删除,抛出自定义异常
       2.没有关系就能删除
    * */
    @Override
    public void deleteById(int id) {
        //根据id查询
        Integer i = checkgroupDao.findByid(id);
        if (i > 0) {
            //代表存在与检查项的关系 不能删除
            throw new HealthException("该检查组已经被使用了，不能删除!");
        }
        //先删除关系表
        checkgroupDao.deleteCheckGroupCheckItem(id);
        //到这说明可以删除
        checkgroupDao.delete(id);
    }

    /*编辑套餐==========数据回显*/
    //回显数据==检查组
    @Override
    public CheckGroup lookdata(int id) {
        CheckGroup checkGroup = checkgroupDao.lookdata(id);
        return checkGroup;
    }

    //回显数据==检查项
    @Override
    public List<Integer> checkitemIds(int checkitemId) {
        return checkgroupDao.checkitemIds(checkitemId);
    }

    //修改编辑检查组
    @Override
    @Transactional
    public void update(CheckGroup checkGroup, Integer[] checkitemIds) {
// 先更新检查组
        checkgroupDao.update(checkGroup);
        // 删除旧关系
        checkgroupDao.deleteCheckGroupCheckItem(checkGroup.getId());
        // 建立新关系
        if(null != checkitemIds){
            for (Integer checkitemId : checkitemIds) {
                checkgroupDao.addItemsAndGroup(checkGroup.getId(), checkitemId);
            }
        }
    }
    /*查询所有的检查项在新增套餐时使用*/
    @Override
    public List<CheckGroup> findAll() {
        return  checkgroupDao.findAll();
    }
}


