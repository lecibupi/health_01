package com.itheima.health.service.impl.securityImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.SecurityDao;
import com.itheima.health.dao.UserDao;
import com.itheima.health.pojo.User;
import com.itheima.health.service.securityService.SecurityServcie;
import org.springframework.beans.factory.annotation.Autowired;
@Service(interfaceClass = SecurityServcie.class)
public class SecurityServcieImpl implements SecurityServcie {
    @Autowired
    private SecurityDao securityDao;

    /**
     * 根据登陆用户名称查询用户权限信息
     * @param username
     * @return
     */
    @Override
    public User securityFindByUsername(String username) {
        return securityDao.findByUsername(username);
    }


}
