package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.dao.OrderDao;
import com.itheima.health.dao.OrdersettingDao;
import com.itheima.health.pojo.Member;
import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderService;
import com.itheima.health.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderService.class)
public class OrderServiceImpl implements OrderService {
@Autowired
    private OrderDao orderDao;
@Autowired
  private MemberDao memberDao;
@Autowired
private OrdersettingDao ordersettingDao;
    /*预约添加*/
    @Override
    @Transactional
    public Order add(Map<String, Object> addMap) {
        /**预约设置的步骤
         * 1先判断当前的日期是否可以预约
         *        不可以预约报错
         *   2再判断当前的日期预约是否已满
         *        已满 报错
         *  3判断是否为会员根据电话号码去Member表查询
         *    不是会员将其将为会员  同时获取主键
         * 4判断是否重复预约通过预约的日期，会员的id，预约的套餐id 查询
         *      重复预约就提示已经预约了
         * 5添加订单
         *  新增订单
         *  6更新当前日期的预约人数 +1
         *
         * */
        //获取用户预约的日期
     String orderDateStr= (String) addMap.get("orderDate");
        Date OrderDate=null;
        try {
            OrderDate = DateUtils.parseString2Date(orderDateStr);
        } catch (Exception e) {
            e.printStackTrace();
            throw new HealthException("预约日期格式不正确");
        }
         /*//1通过日期查询是否可以预约*/
        OrderSetting orderSetting = ordersettingDao.findByOrderDate(OrderDate);
        SimpleDateFormat sf =  new SimpleDateFormat("yyyy-MM-dd");
        if (orderSetting == null) {
         throw  new HealthException("当前日期不能预约，请选择其他日期");
        }
        /*//2判断当前的日期是否预约已满 已预约数>=最大预约数*/
        if(orderSetting.getReservations()>=orderSetting.getNumber()){
           throw  new HealthException("当前日期："+sf.format(OrderDate)+" 预约已满，请选择其他日期");
            //throw new HealthException(MessageConstant.ORDER_FULL);
        }
        /*//3判断是否为会员 ==根据电话号码查询member表*/
        String telephone = (String) addMap.get("telephone");
        Member member = memberDao.findByTelephone(telephone);
        if (null == member){
            //将此用户添加到会员中
            member=new Member();
            member.setName((String) addMap.get("name"));//名字
            member.setSex((String) addMap.get("sex"));//性别
            member.setPhoneNumber((String) addMap.get("telephone"));//电话号码
            member.setIdCard((String) addMap.get("idCard"));//身份证
            member.setRegTime(new Date()); //当前时间
            //将这记录插入到会员表中 同时获取主键
            memberDao.add(member);
        }
       /* //4判断是否重复预约*/
         //根据t_orter表中的 会员id、当前日期、套餐id判断
        Order order =new Order();
        order.setMemberId(member.getId());
        order.setOrderDate(OrderDate);
        order.setSetmealId(Integer.parseInt((String) addMap.get("setmealId")));//套餐id在点击预约的时候在地址中传入了
        //查询这个预约信息是否存在
        List<Order> orderList = orderDao.findByCondition(order);
        if ( orderList.size()>0){
            throw  new HealthException("当前日期已经预约过相同的套餐了，请选择其他套餐获取选择其他日期");
        }
        /*5添加订单*/
          //补全预约信息
        order.setOrderType((String) addMap.get("orderType"));
        order.setOrderStatus(Order.ORDERSTATUS_NO);
        orderDao.add(order);
        /*6更新当前日期的预约人数*/
        //更新的sql语句德拉两种写法：第一种不合适会出现并发问题
        //第一种：先+1在插入 orderSetting.setReservations(orderSetting.getReservations()+1);
        //update t_ordersetting  set reservations=reservations where orderDate=? and number>reservations
        // 100 -> 之前的是多少

        //先执行sql再+1： number>reservations加行锁
        //update t_ordersetting  set reservations=reservations+1 where orderDate=? and number>reservations
        //  log update 多少次 -1
        ordersettingDao.updateNumber(orderSetting);


        return order;
    }
    //预约成功以后跳转到orderSuccess.html 需要将页面数据回显
    @Override
    public Map<String, Object> findByid(int id) {
        Map<String, Object> map = orderDao.findById(id);
        return map;
    }

}

