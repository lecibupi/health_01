package com.itheima.health.service.impl;



import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.OrdersettingDao;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrdersettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.management.InstanceNotFoundException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrdersettingService.class)
public class OrdersettingServiceImpl implements OrdersettingService {
    @Autowired
    private OrdersettingDao ordersettingDao;

//批量上传预约信息
    @Override
    @Transactional
    public void addOrder(List<OrderSetting> orderSettingList){
        /**步骤：
         * 1.判断是上传的文件是否有数据，有数据在进一步操作
         * 2.循环集合。
         * 3.使用上传文件中的日期查询数据库中是否有这个记录，没有这个记录就直接将文件中的记录插入到数据库中
         * 4.当有记录时：判断文件中的可预约数是否大于数据库的已预约人数；
         *                  大于则更新：只有文件中可预约数大于已预约数时才能让已预约的用户体检
         *                   小于则抛异常 ：之前已预约成功的人数会有人没名额
        * */
        if( null != orderSettingList){
            SimpleDateFormat sf =new SimpleDateFormat("yyyy-MM-dd");
            for (OrderSetting orderSetting : orderSettingList) {
                //根据上传的日期查询是预约记录否存在
                OrderSetting byOrderDate = ordersettingDao.findByOrderDate(orderSetting.getOrderDate());
                //判断返回的是否为空
                if (null != byOrderDate){
                    //不为空则判断可以预约的人数是否大于已预约人数
                    if (orderSetting.getNumber() <  byOrderDate.getReservations()){
                        //小于则抛异常
                        throw  new HealthException(sf.format(orderSetting.getOrderDate())+"当前日期的最大预约数小于已预约数，不能修改");

                    }
                    //大于则更新数据
                    ordersettingDao.update(orderSetting);
                }else {
                    //为空直接插入
                    ordersettingDao.addOrderSetting(orderSetting);
                }
            }
        }
    }

    //显示所有的预约信息
    @Override
    public List<Map<String, Integer>> getOrderSettignByMonth(String month) {
        // 拼接开始与结束日期
        String startDate = month + "-01";
         String endDate = month + "-31";
        List<Map<String,Integer>> monthDatas = ordersettingDao.findAllOrder(startDate, endDate);
        return monthDatas;
    }


    @Override
    @Transactional
    public void setMaxNumber(OrderSetting orderSetting) {
        //设置最大预约数
    /**先根据参数查询当前的预约信息 对应的已预约数 ，
       当查询到的预约信息不为空时
                  要设置的可预约必须>查询到的已预约数
                        可预约数大于已预约数：修改最大预约数
                        可预约数小于已预约数：抛异常
          为空时 直接插入

          操作数据库的语句直接调用批量操作时的语句
    * */
        if (null != orderSetting) {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            //根据上传的orderSetting中得日期获取数据库中当前日期的预约信息
            OrderSetting byOrderDate = ordersettingDao.findByOrderDate(orderSetting.getOrderDate());

            //不为空就判断上传中的最大预约数是否大于数据库里的已预约数
            if (null != byOrderDate) {
                //已预约数>最大预约数
                if (byOrderDate.getReservations() > orderSetting.getNumber()) {
                    throw new HealthException(sf.format(orderSetting.getOrderDate()) + "当前日期的最大预约数小于已预约数，不能修改");

                }
                //已预约数<最大预约数
                ordersettingDao.update(orderSetting);
                //如果为空就直接设置
            } else {
                ordersettingDao.addOrderSetting(orderSetting);
            }
        }
    }
}
