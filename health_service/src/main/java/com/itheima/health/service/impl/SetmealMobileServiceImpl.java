package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.SetmealMobileDao;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealMobileService;
import com.itheima.health.util.QiNiuUtils;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SetmealMobileServiceImpl implements SetmealMobileService {
    @Autowired
    private SetmealMobileDao setmealMobileDao;
    @Autowired
    private FreeMarkerConfigurer freemarkerConfig;
    //使用spring的注解注入properties文件中的key的值
    @Value("${out_put_path}")
    private String out_put_path;

    //查询所有套餐
    @Override
    public List<Setmeal> findAllSetmeal()  {
      List<Setmeal> setmeals=  setmealMobileDao.findAllSermeal();
      //这个是使用静态页面的方式 现在使用redis缓存
       /*  //为所有的套餐创建静态页面
      generatwSetmeaList(setmeals);
        //为所有的套餐详情创建静态页面
// 生成所有套餐详情页面静态化
        generateSetmealDetail(setmeals);
        */
        return setmeals;
    }
    /**为每一个套餐详情页面生成静态化页面
     *
     * */
    public void  generateSetmealDetail(List<Setmeal> list){
        if (null != list) {
            try {
                for (Setmeal setmeal : list) {
                    // 获取套餐的详情
                    Setmeal setmealDetail = setmealMobileDao.findById(setmeal.getId());
                    // 图片的完整路径
                    setmealDetail.setImg(QiNiuUtils.DOMAIN + setmealDetail.getImg());
                    // 获取模板
                    Template template = freemarkerConfig.getConfiguration().getTemplate("setmeal_detail.ftl");
                    // 创建数据模型
                    Map<String,Object> dataMap = new HashMap<String,Object>();
                    dataMap.put("setmeal", setmealDetail);
                    // 输出到文件
                    // 构建Writer
                    //String.format("setmeal_%d.html",setmealDetail.getId()：就是将get到的id值赋值到%d的位置
                    String setmealDetailFile = out_put_path + File.separator + String.format("setmeal_%d.html",setmealDetail.getId());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(setmealDetailFile), "utf-8"));
                    // 填充数据
                    template.process(dataMap, writer);
                    // 关闭流
                    writer.flush();
                    writer.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**为所有的套餐页创建静态页面
     步骤：1获取模板
          2构建数据模型Map
          3 构建writer
          4 填充数据、
          5 关闭流

     * */


    public void generatwSetmeaList(List<Setmeal> list){
        try {
            //拼接图片的地址
            for (Setmeal setmeal : list) {
                setmeal.setImg( QiNiuUtils.DOMAIN+setmeal.getImg());
            }


            //获取模板 在WEB-INF、ifl文件下已经为其配置了模板了
            Template template = freemarkerConfig.getConfiguration().getTemplate("mobile_setmeal.ftl");
            //构建数据模型 存入数据
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("setmealList",list);
            //构建writer 文件输出
            //File.separator:就是个/拼接作用
            String setmealListFile = out_put_path+ File.separator+"mobile_setmeal.html";//输出的文件目录
            //使用BufferedWriter效率更高
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(setmealListFile),"utf-8"));
            //填充数据
            template.process(hashMap,writer);
            //关闭流
            writer.flush();//刷新缓冲区
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    //查询对应id的套餐详情 方式一：
    @Override
    public Setmeal findById(int id) {
        //根据套餐id查询对应的套餐信息
        return setmealMobileDao.findById(id);
    }

     //查询对应id的套餐详情 方式二：
    @Override
    public Setmeal findById1(int id) {
        //根据套餐id查询对应的套餐信息
        return setmealMobileDao.findById1(id);
    }
}
