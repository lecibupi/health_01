package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.UserDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import com.itheima.health.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 冯晓东
 * @date 2020/7/26  18:16
 */
@Service(interfaceClass = UserService.class)
public class UserServiceImpl implements UserService {
    //根据登陆用户名称查询用户权限信息
    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
    @Override
    public List<Menu> getMenuByUsername(String username) {
        return userDao.getMenuByUsername(username);
    }
    /********************************************************************/
    @Autowired
    private UserDao userDao;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        if (null != queryPageBean.getQueryString()) {
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString() + "%");
        }

        List<User> userList = userDao.findPage(queryPageBean.getQueryString());
        userList.forEach(user -> {
            if (null != user.getBirthday()) {
                user.setBirthdayStr(simpleDateFormat.format(user.getBirthday()));
            }
        });
        return new PageResult(Long.valueOf(userList.size()), userList);
    }

    @Override
    public List<Role> UsertableData() {
        return userDao.UsertableData();
    }

    @Override
    public Map<String, Object> loadingEdit(Integer id) {
        HashMap<String, Object> map = new HashMap<>();
        //获取用户信息
        User user = userDao.findById(id);
        if (null == user.getBirthday()) {
            throw new HealthException("用户信息不全，请补全用户信息");
        }
        user.setBirthdayStr(simpleDateFormat.format(user.getBirthday()));
        //获取用户的角色信息
        List<Integer> roleIds = userDao.UsercheckitemIds(user.getId());
        map.put("formData", user);
        map.put("tableData", userDao.UsertableData());
        map.put("roleIds", roleIds);
        return map;
    }

    @Override
    @Transactional
    public void UserAdd(User formData, Integer[] roleIds) {
        /*if (null == formData && null == formData.getPassword() || null == formData.getUsername()) {
            throw new HealthException("提交格式不正确");
        }*/
        if (null == roleIds || roleIds.length < 1){
            throw new HealthException("角色信息为必填项");
        }
        if (null == formData.getBirthday()  ){
            throw new HealthException("生日为必填项");
        }
        long BirthdayTime = formData.getBirthday().getTime();
        long nowTime = new Date().getTime();
        if (BirthdayTime > nowTime) {
            throw new HealthException("出生日期非法" + simpleDateFormat.format(formData.getBirthday()));
        }
        //先添加user并获取id
        userDao.UserAdd(formData);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            formData.setBirthday(simpleDateFormat.parse(formData.getBirthdayStr()));
        } catch (Exception e) {}
        //再添加同role的关系关系
        for (Integer roleId : roleIds) {
            userDao.UserRoleRelationAdd(formData.getId(), roleId);
        }
    }

    @Override
    @Transactional
    public void UserEdit(User formData, Integer[] roleIds) {
        if (null == roleIds || roleIds.length < 1 || null == formData && null == formData.getPassword() || null == formData.getUsername()) {
            throw new HealthException("非法格式");
        }
        long BirthdayTime = formData.getBirthday().getTime();
        long nowTime = new Date().getTime();
        if (BirthdayTime > nowTime) {
            throw new HealthException("非法格式" + formData.getBirthday());
        }
        //先维护关系
        userDao.UserRoleRelationDel(formData.getId());
        for (Integer roleId : roleIds) {
            userDao.UserRoleRelationAdd(formData.getId(), roleId);
        }
        //再修改用户
        userDao.UserEdit(formData);
    }

    @Override
    @Transactional
    public void Userdelete(Integer id) {
        //先删除关系
        userDao.UserRoleRelationDel(id);
        //再删除用户
        userDao.Userdelete(id);
    }

    //修改密码
    @Override
    public void setPassword(String username, String newpassword) {
        userDao.setPassword(username,newpassword);
    }

    //获取密码
    @Override
    public String getPassword(String username) {
       User user= userDao.findByUsername(username);
       return user.getPassword();
    }
}
