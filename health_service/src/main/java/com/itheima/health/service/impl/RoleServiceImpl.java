package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.RoleDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RoleService.class)
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        if (null != queryPageBean.getQueryString()) {
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString() + "%");
        }
        List<Role> userList = roleDao.findPage(queryPageBean.getQueryString());
        return new PageResult(Long.valueOf(userList.size()), userList);
    }

    @Override
    public Map<String, Object> RoletableData() {
        Map<String, Object> map = new HashMap<>();
        //先获取关联的权限信息
        List<Permission> permissionList = roleDao.RolePermissionRelation();
        map.put("permissiondata", permissionList);
        //再获取关联的菜单信息
        List<Menu> menuList = roleDao.RoleMenuRelation();
        map.put("menuList", menuList);
        return map;
    }

    @Override
    @Transactional
    public void RoleAdd( List<Integer> menuIds, List<Integer> permissionIds, Role formData) {
        if (null == menuIds || menuIds.size()<1 || null == permissionIds || permissionIds.size()<1) {
            throw new HealthException("角色必须要有权限和菜单信息！！！");
        }
        //先添加角色并获取id
        roleDao.RoleAdd(formData);
        //再添加权限
        for (Integer permissionId : permissionIds) {
            roleDao.RolePermissionRelationAdd(formData.getId(),permissionId);
        }
        //先删除之前的关系，最后再添加菜单关系
        roleDao.RoleMenuRelationDel(formData.getId());
        for (Integer menuId : menuIds) {
            roleDao.RoleMenuRelationAdd(formData.getId(), menuId);
        }
    }

    @Override
    public Map<String, Object> RoletableDataRelation(Integer id) {
        Role role = roleDao.FindByRoleId(id);
        Map<String, Object> map = new HashMap<>();
        List<Integer> permissiondataAdver = new ArrayList<>();
        List<Integer> menuListAdver = new ArrayList<>();
        //先获取角色
        map.put("formData", role);
        //获取所有的权限信息
        List<Permission> permissionList = roleDao.RolePermissionRelation();
        map.put("permissiondata", permissionList);
        //获取关联的权限信息
        permissionList = roleDao.RolePermissionRelationAdver(id);
        permissionList.forEach(permission -> {
            permissiondataAdver.add(permission.getId());
        });
        map.put("permissiondataAdver", permissiondataAdver);
        //获取关联的菜单信息
        List<Menu> menuList = roleDao.RoleMenuRelation();
        map.put("menuList", menuList);
        menuList = roleDao.RoleMenuRelationAdver(id);
        menuList.forEach(menu -> {
            menuListAdver.add(menu.getId());
            List<Menu> children = menu.getChildren();
            if (null != children) {
                for (Menu child : children) {
                    menuListAdver.add(child.getId());
                }
            }
        });
        List<Integer> RoleMenuMaxId = roleDao.RoleMenuMaxId();
        menuListAdver.removeAll(RoleMenuMaxId);
        map.put("menuListAdver", menuListAdver);
        return map;
    }
/*    @Override
    public Map<String, Object> RoletableDataRelation(Integer id) {
        Role role = roleDao.FindByRoleId(id);
        Map<String, Object> map = new HashMap<>();
        List<Integer> permissiondataAdver = roleDao.RolePermissionRelationAdver(role.getId());
        List<Integer> menuListAdver = roleDao.RoleMenuRelationAdver(role.getId());
        //先获取角色
        map.put("formData", role);
        //获取菜单id
        map.put("menuListAdver", menuListAdver);
        //获取权限id
        map.put("permissiondataAdver", permissiondataAdver);
        return map;
    }*/

    @Override
    @Transactional
    public void Roledelete(Integer id) {
        //先删除跟角色关联的权限
        roleDao.RolePermissionRelationDel(id);
        //再删除跟角色关联的菜单
        roleDao.RoleMenuRelationDel(id);
        //再删除角色
        roleDao.RoleDel(id);
    }

    @Override
    @Transactional
    public void RoleEdit(List<Integer> menuIds, List<Integer> permissionIds, Role formData) {
        if (null == menuIds || null == permissionIds || null == formData) {
            throw new HealthException("非法数据提交");
        }

        //先修改角色
        roleDao.RoleEdit(formData);
        //再先删除角色跟权限的关系，再添加角色跟权限的关系
        roleDao.RolePermissionRelationDel(formData.getId());
        List<Integer> RoleMenuMaxId = roleDao.RoleMenuMaxId();
        permissionIds.removeAll(RoleMenuMaxId);
        for (Integer permissionId : permissionIds) {
            roleDao.RolePermissionRelationAdd(formData.getId(), permissionId);
        }
        //最后先删除角色跟菜单的关系，再添加角色跟菜单的关系
        roleDao.RoleMenuRelationDel(formData.getId());
        for (Integer menuId : menuIds) {
            roleDao.RoleMenuRelationAdd(formData.getId(), menuId);
        }
    }
}

