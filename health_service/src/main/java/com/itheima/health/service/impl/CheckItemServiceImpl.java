package com.itheima.health.service.impl;


import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.CheckItemDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    //查询所有
    @Override
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }

    //新增检查项
    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    //分页条件查询检查项
    @Override
    public PageResult<CheckItem> pageFind(QueryPageBean queryPageBean) {
        /*使用插件进行分页查询
        1，在参数中获取当前页与一页几行，赋值给插件的方法
        2。判断是否有条件查询
        3.调用dao层返回page<CheckItem>
        4.创建PageResult<Checkitem>对象 在page中获取总数量、对应页数的检查项信息
            其中总数量是在Threadlooal线程获取的
        * */
        //数据加入进插件的时候就，插件会自动进行分页查询
        PageHelper.startPage( queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //判断是否有查询条件
        if (!StringUtils.isEmpty(queryPageBean.getQueryString())) {
            //有条件就需要拼接查询条件
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString() + "%");
        }
        //调用dao
       Page<CheckItem> page= checkItemDao.pageFindAll(queryPageBean.getQueryString());

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    //编辑--回显数据
    @Override
    public CheckItem findByid(Integer id) {

          CheckItem byId= checkItemDao.findById(id);
        return byId;
    }

    //编辑检查项
    @Override
    public void editService(CheckItem checkItem) {
        checkItemDao.editDao(checkItem);
    }

    @Override
    public void delete(int id) {
        /*先根据检查项的id查询是否与检查组有关系
        有关系抛出异常
        没有关系参数
        * */
        Integer i = checkItemDao.find(id);
        //大于1说明有关联不能删除
        if (i>0) {
       throw new HealthException("该检查项已经被使用了，不能删除!");

        }

        checkItemDao.delete(id);

    }
}
