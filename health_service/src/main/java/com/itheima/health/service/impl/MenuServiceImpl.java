package com.itheima.health.service.impl;

import com.alibaba.druid.sql.visitor.functions.Substring;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.MenuDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;

    //新增菜单
    @Override
    public void add(Menu menu) {

        String path1 = menu.getPath();
    //根据path查询数据库,如果数据库已经存在,则返回"已存在",不存在就返回"可以使用"
        int count = menuDao.findpath(path1);
        if (count>0){
            throw new HealthException("当前路由路径已被使用,不能新增");
        }

        //判断访问路径是否以"/"开头
        if (path1.startsWith("/")){
            //截取"/"到"-"之间的值
            int i = path1.indexOf("-");
            String substring = path1.substring(1, i);

            //得到父菜单的ID
            Integer fuid =menuDao.findFuid(substring);
            if(fuid == null){
                throw new HealthException("当前路径无法使用,父菜单不存在");
            }
            menu.setParentMenuId(fuid);
            menu.setLevel(2);
        }else {
            menu.setParentMenuId(null);
            menu.setLevel(1);
        }
        menuDao.add(menu);
    }

    //分页查询
    @Override
    public PageResult<Menu> findPage(QueryPageBean queryPageBean) {
        // 第一步
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        // 第二步, 紧接着的dao查询语句会被分页
        // 条件查询
        if(!StringUtils.isEmpty(queryPageBean.getQueryString())){
            // queryString是否有值，有值 模糊查询，拼接%
            queryPageBean.setQueryString("%" + queryPageBean.getQueryString()+ "%");
        }
        Page<Menu> page = menuDao.findByCondition(queryPageBean.getQueryString());
        // 线程变量同步技术 ThreadLocal
        // page.getTotal() 总数
        // page.getResult() 分页的结果集
        PageResult<Menu> pageResult = new PageResult<>(page.getTotal(), page.getResult());
        return pageResult;
    }

    //删除菜单
    @Override
    public void deleteById(int id) {
        // 查询要删除的菜单是否被角色使用了
        int count = menuDao.findCountByMenuId(id);
        if(count > 0){
            // 被使用了
            throw new HealthException("该菜单已经被使用了，不能删除!");
        }
        // 没有被使用
        menuDao.deleteById(id);
    }

    //通过ID查询菜单
    @Override
    public Menu findById(int id) {
        return menuDao.findById(id);
    }

    //编辑菜单
    @Override
    public void update(Menu menu) {
        String path1 = menu.getPath();
        //根据path查询数据库,如果数据库已经存在,则返回"已存在",不存在就返回"可以使用"
        int count = menuDao.findpath(path1);
        if (count>2){
            throw new HealthException("当前路由路径已被使用,不能新增");
        }

        //判断访问路径是否以"/"开头
        if (path1.startsWith("/")){
            //截取"/"到"-"之间的值
            int i = path1.indexOf("-");
            String substring = path1.substring(1, i);

            //得到父菜单的ID
            Integer fuid =menuDao.findFuid(substring);
            if(fuid == null){
                throw new HealthException("当前路径无法使用,父菜单不存在");
            }
            menu.setParentMenuId(fuid);
            menu.setLevel(2);
        }else {
            menu.setParentMenuId(null);
            menu.setLevel(1);
        }
        menuDao.update(menu);
    }
}
