package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.SetmealDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.CheckgroupService;
import com.itheima.health.service.SetmealService;
import com.itheima.health.util.QiNiuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetmealService.class)
public class SetmealServiceimpl implements SetmealService {
    @Autowired
   private SetmealDao setmealDao;

    /*添加套餐
    添加套餐的信息同时还需要添加套餐与检查组的关系
    需要加上事物
    * */
    @Override
    @Transactional
    public void add(Setmeal setmeal,Integer[] checkgroupIds ) {
        //添加套餐的信息 同时获取其主键自增
      setmealDao.add(setmeal);
      //获取主键id
        Integer setmealid = setmeal.getId();
        //判断是否有选择检查组
        if(null != checkgroupIds){
            //添加检查组与套餐的关系
            for (Integer checkgroupId : checkgroupIds) {

                setmealDao.addSetmealAndChenkgroup(setmealid,checkgroupId);
            }
        }

    }

    //分页条件查询
    @Override
    public PageResult<Setmeal> findPage(QueryPageBean queryPageBean) {
        //使用插件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //判断是有条件
        if(!StringUtils.isEmpty(queryPageBean.getQueryString())){
            queryPageBean.setQueryString("%"+queryPageBean.getQueryString()+"%");
        }
        Page<Setmeal> page= setmealDao.findPage(queryPageBean.getQueryString());


        return new PageResult<>(page.getTotal(),page.getResult());
    }
 //删除套餐
    @Override
    @Transactional
    public void deleteById(int setmearlId) {

        Integer i = setmealDao.find(setmearlId);
        //大于1说明有关联不能删除
        if (i>0) {
            throw new HealthException("该检查组已经被使用了，不能删除!");
        }
        //- 先删除套餐与检查组的关系
        setmealDao.deleteSetmealCheckgroup(setmearlId);
        //删除
        setmealDao.delete(setmearlId);
    }
    //回显数据==套餐信息
    @Override
    public Setmeal lookdata(int id) {

        return setmealDao.lookdata(id);
    }

    //数据回显 --选中的检查组
    @Override
    public List<Integer>  findAllCheckgroudId(int setmearlId) {
       return setmealDao.findAllCheckgroudId(setmearlId);


    }

    //提交修改
    @Override
    @Transactional
    public void edit(Integer[] checkgroudIds ,Setmeal setmeal) {
        /*修改套餐
        1.添加套餐与检查组的关系
        2.删除套餐与检查组的关系表
        3.更新套餐与检查组的关系
        * */
        //更新套餐
        setmealDao.edit(setmeal);

        //删除套餐与检查组关系
        setmealDao.deleteSetmealCheckgroup(setmeal.getId());
        //判断套餐里面是否有选检查组
        //有就更新检查组与套餐的关系
        if (checkgroudIds !=null){
            for (Integer checkgroudId : checkgroudIds) {
              setmealDao.addSetmealAndChenkgroup(setmeal.getId(),checkgroudId);
            }
        }

    }


}
