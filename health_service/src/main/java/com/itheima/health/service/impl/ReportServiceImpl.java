package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.dao.OrderDao;
import com.itheima.health.service.ReportService;
import com.itheima.health.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;


    @Override
    public Map<String, Object> getBusinessReport() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
      //获取周一
       String Monday = sf.format(DateUtils.getThisWeekMonday());
       //获取周日
        String sunday = sf.format(DateUtils.getSundayOfThisWeek());
        //获取月份的第一天
        String startDay = sf.format(DateUtils.getFirstDay4ThisMonth());
        //获取月份的最后一天
        String endDay = sf.format(DateUtils.getLastDay4ThisMonth());

        //reportDate（当前时间）
        Date date = new Date();
        String reportDate = sf.format(date);
        //=================================会员========================
        //todayNewMember（今日新增会员数）
        Integer todayNewMember = memberDao.findMemberCountByDate(reportDate);
        //totalMember（总会员数 ）
        Integer totalMember = memberDao.findMemberTotalCount();
        //thisWeekNewMember（本周新增会员数，本周一开始，不需要加周日因为不能提前注册成为会员）
        Integer thisWeekNewMember = memberDao.findMemberCountAfterDate(Monday);
        //thisMonthNewMember（本月新增会员数 从本月1号开始）
        Integer thisMonthNewMember = memberDao.findMemberCountAfterDate(startDay);

        //=================================预约数========================
        //todayOrderNumber（今日预约数）
        Integer todayOrderNumber = orderDao.findOrderCountByDate(reportDate);
        //todayVisitsNumber（今日到诊数）
        Integer todayVisitsNumber = orderDao.findVisitsCountByDate(reportDate);
        //thisWeekOrderNumber（本周预约数）(从本周的周一到周日，因为可以提前预约以后的日期)
        Integer thisWeekOrderNumber = orderDao.findOrderCountBetweenDate(Monday,sunday);
        //thisWeekVisitsNumber（本周到诊数）（大于本周一）
        Integer  thisWeekVisitsNumber = orderDao.findVisitsCountAfterDate(Monday);
        //thisMonthOrderNumber（本月预约数）（这月1号-31号，可以提前预约所以要加上条件 ）
        Integer thisMonthOrderNumber = orderDao.findOrderCountBetweenDate(startDay,endDay);
        //thisMonthVisitsNumber（本月到诊数 从本月1号开始）
        Integer thisMonthVisitsNumber = orderDao.findVisitsCountAfterDate(startDay);


        //=================================套餐========================
        //hotSetmeal（热门套餐（
        List<Map> hotSetmeal = orderDao.findHotSetmeal();

        //封装返回数据
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("reportDate",reportDate);
        resultMap.put("todayNewMember",todayNewMember);
        resultMap.put("totalMember",totalMember);
        resultMap.put("thisWeekNewMember",thisWeekNewMember);
        resultMap.put("thisMonthNewMember",thisMonthNewMember);
        resultMap.put("todayOrderNumber",todayOrderNumber);
        resultMap.put("todayVisitsNumber",todayVisitsNumber);
        resultMap.put("thisWeekOrderNumber",thisWeekOrderNumber);
        resultMap.put("thisWeekVisitsNumber",thisWeekVisitsNumber);
        resultMap.put("thisMonthOrderNumber",thisMonthOrderNumber);
        resultMap.put("thisMonthVisitsNumber",thisMonthVisitsNumber);
        resultMap.put("hotSetmeal",hotSetmeal);
        return resultMap;

    }
    /**
     * 按照日期显示数据
     * @param beginTimeStr
     * @param endTimeStr
     * @return
     */
    @Override
    public List<Map<String, Object>> getMonthDate(String beginTimeStr, String endTimeStr) {


        List<Map<String, Object>> datelist = memberDao.getMonthDate(beginTimeStr, endTimeStr);
        CopyOnWriteArrayList<Map<String, Object>> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        for (Map<String, Object> stringObjectMap : datelist) {
            copyOnWriteArrayList.add(stringObjectMap);
        };
        Calendar c = Calendar.getInstance();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        //开始时间必须小于结束时间
        Date beginDate = null;
        Date endDate = null;
        try {
            beginDate = dateFormat1.parse(beginTimeStr);
            endDate = dateFormat1.parse(endTimeStr);
            Date date = beginDate;
            while (!date.equals(endDate)) {
                ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();
                Iterator<Map<String, Object>> iterator = copyOnWriteArrayList.iterator();
                String datetime = dateFormat1.format(date);
                if(copyOnWriteArrayList.size()==0){
                    map.put("date",datetime);
                    map.put("value",0);
                    copyOnWriteArrayList.add(map);
                }
                else{
                    if (iterator.hasNext()){
                        Map<String, Object> stringObjectMap = iterator.next();

                        if (stringObjectMap.containsValue(datetime)) {
                            continue;
                        }else {

                            map.put("date",datetime);
                            map.put("value",0);
                            copyOnWriteArrayList.add(map);
                        }
                    } }
                c.setTime(date);
                c.add(Calendar.DATE, 1);
                date = c.getTime();
            }
            return copyOnWriteArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }


    }

    /***
     * 按照月份来显示数据;
     * @param beginTimeStr
     * @param endTimeStr
     * @return
     */

    @Override
    public Map<String, Object> getYearData(String beginTimeStr, String endTimeStr) {

        List<Map<String, Object>> maps = memberDao.getYearData(beginTimeStr, endTimeStr);
        //处理返回数据
        HashMap<String, Object> hashMap = new HashMap<>();
        ArrayList<String> monthlist = new ArrayList<>();
        ArrayList<Long> monthcount = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            String month = (String) map.get("date");
            monthlist.add(month+"-月份");
            Long value = (Long) map.get("value");
            monthcount.add(value);
        }
        hashMap.put("date",monthlist);
        hashMap.put("value",monthcount);

        return hashMap;
    }
}
