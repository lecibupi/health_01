package com.itheima.health.service;



import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.dao.UserDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Menu;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * @author 冯晓东
 * @date 2020/7/26  18:15
 */
public interface UserService {
    User findByUsername(String username);

    List<Menu> getMenuByUsername(String username);

    PageResult findPage(QueryPageBean queryPageBean);

    List<Role> UsertableData();

    Map<String, Object> loadingEdit(Integer id) throws HealthException;

    void UserAdd(User formData, Integer[] roleIds) throws HealthException;

    void UserEdit(User formData, Integer[] roleIds) throws HealthException;

    void Userdelete(Integer id);
	
	//修改密码
    void setPassword(String username, String newpassword) throws Exception;
//获取密码
    String getPassword(String username);
}
