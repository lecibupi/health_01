package com.itheima.health.service;

import java.util.List;
import java.util.Map;

public interface ReportService {
    //运用数据
    Map<String, Object> getBusinessReport();
    List<Map<String,Object>> getMonthDate(String beginTimeStr, String endTimeStr);

    Map<String,Object> getYearData(String beginTimeStr, String endTimeStr);
}
