package com.itheima.health.service;



import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;

import java.util.List;

public interface CheckItemService {
    List<CheckItem> findAll();

    //新增检查项
    void add(CheckItem checkItem);

    //检查项分页条件查询
    PageResult<CheckItem> pageFind(QueryPageBean queryPageBean);

    CheckItem findByid(Integer id);

    void editService(CheckItem checkItem);

    //当使用自定义异常时dubbo框架需要在接口的方法上抛出异常 不然抛出实现类对应方法中抛出的异常会被转换成字符串
    void delete(int id)throws HealthException;

}
