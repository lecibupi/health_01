package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.MyExecption.HealthException;

import java.util.List;

public interface SetmealService {
    //新增套餐
    void add(Setmeal setmeal,Integer[] checkgroupIds );
   //分页条件查询
    PageResult<Setmeal> findPage(QueryPageBean queryPageBean);
     //删除
    void deleteById(int id) throws HealthException;
    //回显数据==套餐信息
    Setmeal lookdata(int setmealId);
    //数据回显 --选中的检查组
    List<Integer>  findAllCheckgroudId(int setmearlId);
  //提交数据信息
    void edit(Integer[] checkgroudIds ,Setmeal setmeal);
}
