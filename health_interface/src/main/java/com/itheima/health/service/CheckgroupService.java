package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;

import java.util.List;

public interface CheckgroupService {

    //新增检查组
     void add(CheckGroup checkGroup, Integer[] checkitemIds) ;

    //检查组分页条件查询
    PageResult<CheckGroup> findPage(QueryPageBean queryPageBean);

    //删除检查组
    void deleteById(int id) throws HealthException;

    //回显数据=检查组
    CheckGroup lookdata(int id);
    //回显数据=检查项
    List<Integer> checkitemIds (int checkitemIds);

    //修改检查组
    void update(CheckGroup checkGroup, Integer[] checkitemIds);

    List<CheckGroup> findAll();
}
