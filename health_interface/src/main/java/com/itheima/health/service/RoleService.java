package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    PageResult findPage(QueryPageBean queryPageBean);

    Map<String, Object> RoletableData();

    void RoleAdd(List<Integer> menuIds, List<Integer> permissionIds, Role formData) throws  HealthException;

    Map<String, Object> RoletableDataRelation(Integer id) ;

    void Roledelete(Integer id);

    void RoleEdit(List<Integer> menuIds, List<Integer> permissionIds, Role formData) throws  HealthException;
}
