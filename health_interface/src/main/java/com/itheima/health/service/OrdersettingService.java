package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrdersettingService {
    //批量导入预约文件
    void addOrder(List<OrderSetting> orderSettingList) throws HealthException;
       //显示所有的预约信息
    List<Map<String, Integer>> getOrderSettignByMonth(String month);
   //设置最大预约数
    void setMaxNumber(OrderSetting orderSetting) throws HealthException;
}
