package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.pojo.Order;

import java.util.Map;

/**
 * @author Administrator
 */
public interface OrderService {
    //添加预约
    Order add(Map<String, Object> addMap) throws HealthException;
//预约成功以后跳转到orderSuccess.html 需要将页面数据回显
    Map<String, Object> findByid(int id);
}
