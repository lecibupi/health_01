package com.itheima.health.service.securityService;

import com.itheima.health.pojo.User;

public interface SecurityServcie {
    //权限管理
    User securityFindByUsername(String username);
}
