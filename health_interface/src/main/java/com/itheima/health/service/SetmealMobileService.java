package com.itheima.health.service;

import com.itheima.health.pojo.Setmeal;

import java.util.List;

public interface SetmealMobileService {
    //查询所有的套餐
    List<Setmeal> findAllSetmeal() ;
  //根据套餐id查询对应的套餐信息
    Setmeal findById(int id);
    //根据套餐id查询对应的套餐信息
     /*方式二
     * */
    Setmeal findById1(int id);
}
