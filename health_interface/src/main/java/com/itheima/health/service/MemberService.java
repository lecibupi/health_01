package com.itheima.health.service;

import com.itheima.health.pojo.Member;

import java.util.List;
import java.util.Map;


public interface MemberService  {
    //根据电话查询是否是会员
    Member findMember(String telephone);

    //添加会员
    void add(Member member);

    //查询每月注册的会员
    List<Integer> findReportMember(List<String> months);

    //套餐饼图 查询套餐在订单表里的数量
    List<Map<String,Object>> findsetmealCount();
//会员男女饼状图，获取会员男女数量
    List<Map<String, Object>> getMemberReportGender();
//会员年龄段占比饼状图，获取各个年龄段会员数量
    Map<String, Integer> getMemberReportAge();
}
