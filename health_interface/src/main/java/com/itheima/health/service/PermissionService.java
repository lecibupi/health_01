package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Permission;

public interface PermissionService {

    //新增权限数据
    void add(Permission permission);

    //分页查询
    PageResult<Permission> findPage(QueryPageBean queryPageBean);

    //删除权限
    void deleteById(int id) throws HealthException;

    //通过id查询权限
    Permission findById(int id);

    //编辑权限
    void update(Permission permission);
}
