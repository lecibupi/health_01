package com.itheima.health.service;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Menu;

public interface MenuService {
    //新增菜单
    void add(Menu menu) throws HealthException;

    //分页查询
    PageResult<Menu> findPage(QueryPageBean queryPageBean);

    //删除菜单
    void deleteById(int id) throws HealthException;

    //通过ID查询菜单
    Menu findById(int id);

    //编辑菜单
    void update(Menu menu) throws HealthException;
}
