package com.itheima.health.util;

import jdk.nashorn.internal.runtime.regexp.RegExp;

import java.util.regex.Pattern;

/**
 * @program: health_01
 * @ClassName: PartenUtils
 * @description: ${description}
 * @author: Administrator
 * @create: 2020-08-10 20:15
 **/
public class PartenUtils {


    public static void main(String[] args) {

        String content = "/2-5";
//        String pattern = "^/?[1-9]{1,2}?[^A-Za-z0-9_]{1}[1-9]{1,2}$";
        String pattern = "^/?[1-9]{1,2}?-{1}[1-9]{1}$";
        boolean isMatch = Pattern.matches(pattern, content);
        System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);

    }
}
