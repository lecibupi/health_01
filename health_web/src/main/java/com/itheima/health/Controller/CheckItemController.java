package com.itheima.health.Controller;




import com.alibaba.dubbo.config.annotation.Reference;

import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckItemService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {

@Reference
private CheckItemService checkItemService;

    //查询所有检查项
    /*在检查组新增时，单击新增的时就将检查项的信息展示出来
    * */
    @RequestMapping ("/findAll")
    //@ResponseBody //使用了@RestController时ResponseBody可以省略
    public Result faindAll(){

        List<CheckItem> checkItems = checkItemService.findAll();
      return  new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkItems);

    }
    //新增检查项
    @PostMapping("/add")
     @PreAuthorize("hasAnyAuthority('CHECKITEM_ADD')")//使用注解为此方法配置权限
        public Result add(@RequestBody CheckItem checkItem){

        checkItemService.add(checkItem);


        return new Result(true,MessageConstant.ADD_CHECKITEM_SUCCESS);
    }
    //分页条件查询检查项
    @PostMapping("pageFindAll")
    public Result pageFind(@RequestBody QueryPageBean queryPageBean){
        //调用service
       PageResult<CheckItem> checkItems= checkItemService.pageFind(queryPageBean);
          return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItems);
    }
    //编辑检查项数据回显
    @GetMapping("/lookdata")
    public Result LookData( int id){
          //调用service
       CheckItem checkItem= checkItemService.findByid(id);
       return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
    }
    //编辑检查项
    @PostMapping("/edit")
    public Result edit(@RequestBody CheckItem checkItem){
        checkItemService.editService(checkItem);
        return new Result(true,MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }
    //删除检查项
    @GetMapping("/delete")
    public Result delete  (int id) {

            checkItemService.delete(id);
            return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);

    }

}
