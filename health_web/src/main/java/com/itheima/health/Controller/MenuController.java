package com.itheima.health.Controller;
//菜单数据动态维护（增删改查）

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.MenuService;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.POST;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Reference
    private MenuService menuService;

    //新增菜单
    @PostMapping("/add")
    public Result add(@RequestBody Menu menu){
        menuService.add(menu);
        return new Result(true,"新增菜单成功");
    }

    //分页查询
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        // 调用服务分页查询
        PageResult<Menu> pageResult = menuService.findPage(queryPageBean);
        // 包装到Result
        return new Result(true,"查询菜单成功",pageResult);
    }

    //删除菜单
    @PostMapping("/delete")
    public Result delete(int id){
        menuService.deleteById(id);
        return new Result(true,"删除菜单成功");
    }

    //通过ID查询菜单
    @GetMapping("/findById")
    public Result findById(int id){
        // 调用业务通过id查询检查项
        Menu menu = menuService.findById(id);
        return new Result(true, "查询菜单成功",menu);
    }

    //编辑菜单
    @PostMapping("/update")
    public Result update(@RequestBody Menu menu){
        // 调用服务更新
        menuService.update(menu);
        // 返回结果给页面
        return new Result(true,"编辑菜单成功");
    }

    //根据path查询数据库,如果数据库已经存在,则返回"已存在",不存在就返回"可以使用"
    /*@PostMapping("/addpath")
    public Result addpath(String path){
        menuService.
    }*/

}
