package com.itheima.health.Controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * @author 冯晓东
 * @date 2020/7/26  22:03
 */

@RestController
@RequestMapping("/User")
public class UserController {
    @RequestMapping("/getUsername")
    public Result getUsername() {
        //.getContext().安全容器对象
        //getAuthentication():认证对象
        //.getPrincipal();当前用户对象 user
        try {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, user.getUsername());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }
    }
    @GetMapping("/getMenuByUsername")
    public Result getMenuByUsername() {
        try {
            //从security安全容器中获取username
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<Menu> menus = userService.getMenuByUsername(user.getUsername());
            return new Result(true, MessageConstant.GET_MENU_SUCCESS, menus);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }
    }
/********************************************************************/
    private static String ResultInfo = "数据成功返回";
    @Reference
    private UserService userService;

    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {
        return new Result(true, ResultInfo, userService.findPage(queryPageBean));
    }

    @GetMapping("/UsertableData")
    public Result UsertableData() {
        return new Result(true, ResultInfo, userService.UsertableData());
    }

    @GetMapping("/loadingEdit")
    public Result loadingEdit(Integer id) {
        return new Result(true, "数据成功返回", userService.loadingEdit(id));
    }

    @PostMapping("/UserAdd")
    public Result UserAdd(@RequestBody com.itheima.health.pojo.User formData, Integer[] roleIds) {
            userService.UserAdd(formData, roleIds);
            return new Result(true, "用户新增成功");

    }

    @PostMapping("/UserEdit")
    public Result UserEdit(@RequestBody com.itheima.health.pojo.User formData, Integer[] roleIds) {
        try {
            ResultInfo = "用户修改成功";
            userService.UserEdit(formData, roleIds);
            return new Result(true, ResultInfo);
        } catch (Exception e) {
            ResultInfo = "用户修改失败";
            return new Result(false, ResultInfo);
        }
    }

    @GetMapping("/Userdelete")
    public Result Userdelete(Integer id) {
        try {
            ResultInfo = "用户删除成功";
            userService.Userdelete(id);
            return new Result(true, ResultInfo);
        } catch (Exception e) {
            return new Result(false, "用户删除失败");
        }
    }

//*************************************************************************************

  /*修改密码*/
    @PostMapping("/setPassword")
    public Result setPassword(@RequestBody Map<String,String> aa, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //获取用户名
        String username = user.getUsername();
        //String password = user.getPassword();
        //获取用户密码
        String password=userService.getPassword(username);
        //BCryptPasswordEncoder
        BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        if ((!encoder.matches(aa.get("password"),password))||!(aa.get("conformedPassword").equals(aa.get("newPassword")))){
            return new Result(false,"密码输入错误");
        }
        if (aa.get("password").equals(aa.get("newPassword"))||encoder.matches(aa.get("newPassword"),password)){
            return new Result(false,"密码相同无需修改");
        }
        if (encoder.matches(aa.get("password"),password)){
            //将新密码进行加密
            String newpassword = encoder.encode(aa.get("newPassword"));
            userService.setPassword(username,newpassword);
          SecurityContextHolder.clearContext();
          /*  HttpSession session = request.getSession();
            if (session!=null){
                session.invalidate();
            }
            Cookie[] cookies = request.getCookies();
            if (cookies!=null){
                for (Cookie cookie : cookies) {
                    cookie.setMaxAge(0);
                }
            }*/
            return new Result(true,"修改成功");
        }
        return new Result(false,"修改密码失败");
    }
}




