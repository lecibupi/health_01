package com.itheima.health.Controller;
//权限数据动态维护（增删改查）


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Permission;
import com.itheima.health.service.PermissionService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Reference
    private PermissionService permissionService;

    //新增权限数据
    @PostMapping("/add")
    public Result add(@RequestBody Permission permission){
        permissionService.add(permission);
        return new Result(true,"新增权限成功");
    }

    //分页查询
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult<Permission> pageResult = permissionService.findPage(queryPageBean);
        return new Result(true,"权限查询成功",pageResult);
    }

    //删除权限
    @PostMapping("/delete")
    public Result delete(int id){
        permissionService.deleteById(id);
        return new Result(true,"删除权限成功");
    }

    //通过id查询权限
    @GetMapping("/findById")
    public Result findById(int id){
        Permission permission = permissionService.findById(id);
        return new Result(true,"查询权限成功",permission);
    }

    //编辑权限
    @PostMapping("/update")
    public Result update(@RequestBody Permission permission){
        permissionService.update(permission);
        return new Result(true,"编辑权限成功");
    }
}
