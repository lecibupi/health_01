package com.itheima.health.Controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.service.MemberService;
import com.itheima.health.service.ReportService;

import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.management.InstanceNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/report")
public class ReportController {
    @Reference
    private MemberService memberService;
    @Reference
  private ReportService reportService;

    /*
    * @Description: 会员年龄段占比
    * @Param: []
    * @create: 2020/8/9 11:24
    * @return: 14265
    */
    @RequestMapping("/getMemberReportAge")
    public Result getMemberReportAge(){
        List<String> memberAges=new ArrayList<>();
        memberAges.add("0-15");
        memberAges.add("16-30");
        memberAges.add("31-45");
        memberAges.add("46-60");
        memberAges.add("61-75");
        memberAges.add("76-90");
        memberAges.add("90以上");
        memberAges.add("未填写");
        Map<String,Object> map=null;
        List<Map<String,Object>> memberCount=new ArrayList<>();
        Map<String,Integer> maps=memberService.getMemberReportAge();
        for (String name : memberAges) {
            map=new HashMap<>();
            map.put("name",name);
            map.put("value",maps.get(name));
            memberCount.add(map);
        }
        Map<String,Object> objectMap=new HashMap<>();
        objectMap.put("memberAges",memberAges);
        objectMap.put("memberCount",memberCount);
        return new Result(true,"查询会员年龄段占比成功",objectMap);
    }
/*
* @Description: 会员男女占比
* @Param:
* @create: 2020/8/9 9:42
* @return: 男女，男女数量
*/
@RequestMapping("/getMemberReportGender")
public Result getMemberReportGender(){
    List<String> memberGenders=new ArrayList<>();
  //获取会员男女的数量
    List<Map<String,Object>> memberCounts=memberService.getMemberReportGender();
    for (Map<String, Object> memberCount : memberCounts) {
        if (memberCount.get("name").equals("1")){
            memberCount.put("name","男");
            memberGenders.add("男");//1
        }else {
            memberCount.put("name","女");
            memberGenders.add("女");//2
        }
    }
    Map<String,Object> objectMap=new HashMap<>();
    objectMap.put("memberGenders",memberGenders);
    objectMap.put("memberCount",memberCounts);
    return new Result(true,"查询会员性别占比成功",objectMap);
}
    /**
     * 过去一面的会员注册折线图
     */
    @GetMapping("/getMemberReport")
    public Result getMember() {
        /*获取过去一年的会员注册信息 ==== 获取过去一年
         //获取过去十二个月：
          先将月份-12，在循环加1 添加进集合就获取到了
         * */
        List<String> months = new ArrayList<String>();
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM");
        //获取过去一年的每一个月
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -12);
        for (int i = 0; i < 12; i++) {
            calendar.add(Calendar.MONTH, 1);
            //获取每一个月的日期并且格式化 ：只有年月
            months.add(sd.format(calendar.getTime()));
        }

        //调用service获取每个月的会员注册数

        List<Integer> memberCount = memberService.findReportMember(months);
        //返回客户端
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("months", months);
        resultMap.put("memberCount", memberCount);

        return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS, resultMap);
    }

    //查询数据库中套餐的预约占比
    @GetMapping("/getSetmealReport")
    public Result getSetmealReport() {
        /**需要返回的数据:套餐名集合+订单表中的套餐名与数量
         *     需要返回的数据格式map(套餐名:name,map(套餐名：name,数量：xx))
         *  先获取订单表中的套餐数量 ；就能获取套餐名
         * */
        //存套餐名
        List<String> setmealNames = new ArrayList<>();
        //调service查询套餐数量、
        List<Map<String, Object>> setmealCount = memberService.findsetmealCount();

        if (setmealCount != null) {
            //循环获取套餐名
            for (Map<String, Object> sc : setmealCount) {
                String name = (String) sc.get("name"); // (String(name:入职无忧体检)，object(value:1))
                setmealNames.add(name);

            }
        }
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("setmealNames", setmealNames);
        resultMap.put("setmealCount", setmealCount);
        return new Result(true, MessageConstant.GET_SETMEAL_COUNT_REPORT_SUCCESS, resultMap);

    }

    //运营数据统计
    @GetMapping("/getBusinessReportData")
    public Result getBusinessReportData() {
        //调用service 返回的数据格式 map
        Map<String, Object> businessReport = reportService.getBusinessReport();
        return new Result(true, MessageConstant.GET_BUSINESS_REPORT_SUCCESS,businessReport);

    }
    //=========================================================
    @PostMapping("/getReportData")
    public Result getReportData(@Param("stamp") @RequestBody Long[]  stamp){

        if(stamp.length==0){
            return new Result(false,"数据参数异常");
        }
        long endstamp = stamp[1];
        long beginstamp = stamp[0];
        if(endstamp<beginstamp){
            return new Result(false,"结束时间不能小于开始时间");
        }



        //小于1个月,按照日期来显示
        long monthTime =2592000000L;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //转换时间
        Date beginDate = new Date(beginstamp);
        Date endDate = new Date(endstamp);
        //转成时间字符串
        String beginTimeStr = simpleDateFormat.format(beginstamp);
        String endTimeStr = simpleDateFormat.format(endDate);

        //处理数据
        List<Map<String, Object>> maps =null;
        List<String>  name =new ArrayList<>();
        List<Integer>  value =new ArrayList<>();
        Map<String,Object> hash =new HashMap<>();
        if((endstamp-beginstamp)<=monthTime){
            //拼装数据的方式
            maps = reportService.getMonthDate(beginTimeStr,endTimeStr);
            for (Map<String, Object> map : maps) {
                for (String key : map.keySet()) {
                    if (key.equals("date")){
                        name.add(String.valueOf( map.get("date")));
                    }else {

                        value.add(Integer.parseInt(String.valueOf(map.get("value")) ));
                    }
                } }
            hash.put("date",name);
            hash.put("value",value);
            return new Result(true,"获取成功",hash);
            //大于1个月,按照月份来显示
        } else {

            Map<String, Object> yearData = reportService.getYearData(beginTimeStr, endTimeStr);
            return new Result(true,"成功",yearData);
        }

    }


    //文件下载
    @GetMapping("/exportBusinessReport")
    public Result exportBusinessReport(HttpServletRequest request, HttpServletResponse response){
        try {
        // 1. 获取模板文件
            String realPath = request.getSession().getServletContext().getRealPath("/template/report_template.xlsx");
            // 2. 创建工作簿
        XSSFWorkbook wk = new XSSFWorkbook(realPath);
        // 3. 获取运营数据
        Map<String, Object> report = reportService.getBusinessReport();
      // 4. 创建一个工作表
        XSSFSheet  sheet= wk.getSheetAt(0);
        // 5. 根据模板填充运营数据
         //=================时间==========================
          sheet.getRow(2).getCell(5).setCellValue((String) report.get("reportDate"));
        //=================会员==========================
        sheet.getRow(4).getCell(5).setCellValue((Integer) report.get("todayNewMember"));
        sheet.getRow(4).getCell(7).setCellValue((Integer) report.get("totalMember"));
        sheet.getRow(5).getCell(5).setCellValue((Integer) report.get("thisWeekNewMember"));
        sheet.getRow(5).getCell(7).setCellValue((Integer) report.get("thisMonthNewMember"));
        //=================预约==========================
        sheet.getRow(7).getCell(5).setCellValue((Integer) report.get("todayOrderNumber"));
        sheet.getRow(7).getCell(7).setCellValue((Integer) report.get("todayVisitsNumber"));
        sheet.getRow(8).getCell(5).setCellValue((Integer) report.get("thisWeekOrderNumber"));
        sheet.getRow(8).getCell(7).setCellValue((Integer) report.get("thisWeekVisitsNumber"));
        sheet.getRow(9).getCell(5).setCellValue((Integer) report.get("thisMonthOrderNumber"));
        sheet.getRow(9).getCell(7).setCellValue((Integer) report.get("thisMonthVisitsNumber"));
        //=================热门套餐==========================
        List<Map<String,Object>> hotSetmeal = (List<Map<String,Object>>)report.get("hotSetmeal");
        int rowIndex = 12;
        for (Map<String, Object> map : hotSetmeal) {
            //获取行对象 
            Row row = sheet.getRow(rowIndex);
            row.getCell(4).setCellValue((String)map.get("name"));
            row.getCell(5).setCellValue((Long)map.get("count"));
            BigDecimal proportion = (BigDecimal)map.get("proportion");
            row.getCell(6).setCellValue(proportion.doubleValue());
            row.getCell(7).setCellValue((String)map.get("remark"));
            rowIndex++;
        }
        // 6. 设置ContextType application/vnd.ms-excel
        //将返回体设置成为文件下载并且文件的类型是excel格式
           response.setContentType("application/vnd.ms-excel");
        //解决文件名中文乱码问题 http 采用的是iso-8859-1编码
        String filename="运营数据.xlsx";
        filename = new String(filename.getBytes(),"iso-8859-1");
      // 7. 设置请求头 Content-Disposition, attachement;filename=文件名
      response.setHeader("Content-Disposition","attachement;filename=" +filename);
      //使用字节输出流发送文件
            wk.write(response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /*pdf这个功能不需要使用注释掉，怕你们的idea没有pdf的maven*/

   /* *//**
     * 导出运营统计数据报表PDF
     *//*
    @GetMapping("/exportBusinessReportPdf")
    public Result exportBusinessReportPdf(HttpServletRequest req, HttpServletResponse res){
        // 获取模板的路径, getRealPath("/") 相当于到webapp目录下
        String basePath = req.getSession().getServletContext().getRealPath("/template");
        // jrxml路径
        String jrxml = basePath + File.separator + "report_business.jrxml";
        // jasper路径
        String jasper = basePath + File.separator + "report_business.jasper";

        try {
            // 编译模板
            JasperCompileManager.compileReportToFile(jrxml, jasper);
            Map<String, Object> businessReport = reportService.getBusinessReport();
            // 热门套餐(list -> Detail1)
            List<Map<String,Object>> hotSetmeals = (List<Map<String,Object>>)businessReport.get("hotSetmeal");
            // 填充数据 JRBeanCollectionDataSource自定义数据
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, businessReport, new JRBeanCollectionDataSource(hotSetmeals));
            res.setContentType("application/pdf");
            res.setHeader("Content-Disposition","attachement;filename=businessReport.pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, res.getOutputStream());
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(false,"导出运营数据统计pdf失败");
    }*/

}


