package com.itheima.health.Controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import com.itheima.health.util.QiNiuUtils;
import org.junit.validator.PublicClassValidator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.management.relation.RelationSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/setmeal")


public class SetmealConntroller {
    @Reference
    private SetmealService setmealService;


    @RequestMapping("/upload")
    public Result upload(MultipartFile imgFile) {
      /*文件上传
     参数MultipartFile imgFile形参名必须要与提交的一致
    1.获取文件名 切割得到后缀 用于给生成文件名后进行拼接
    2.生成唯一文件名UUID拼接后缀
    3.获取URL地址拼接文件名
    4.上传文件
    5.封装数据成map；返回值里包括文件名、上传的路径
    * */
        //获取文件名后切割得到后缀
        //文件名
        String originalFilename = imgFile.getOriginalFilename();
        //切割
        String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
        //生成文件名
        String imgName = UUID.randomUUID() + substring;
        //上传文件
        try {
            QiNiuUtils.uploadViaByte(imgFile.getBytes(), imgName);
        } catch (IOException e) {
            e.printStackTrace();

            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
        //封装返回的数据
        Map<String, String> resultMap = new HashMap<>();

        resultMap.put("imgName", imgName);
        //封装图片上传的路径
        resultMap.put("imgUrl", QiNiuUtils.DOMAIN);
        return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS, resultMap);
    }

    /*       功能描述:添加套餐
     */
    @PostMapping("/add")
    public Result add(@RequestBody Setmeal setmeal, Integer[] checkgroupIds) {
        // 调用服务添加
        setmealService.add(setmeal, checkgroupIds);
        return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);
    }

    /*分页查询
     * */
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {

        //完成功能
        PageResult<Setmeal> pageResult = setmealService.findPage(queryPageBean);
        return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, pageResult);
    }

    /*删除套餐*/
    @GetMapping("/delete")
    public Result delete(int id) {
        // 服务删除
        setmealService.deleteById(id);
        return new Result(true, MessageConstant.DELETE_SETMEAL_SUCCESS);
    }

    /* 编辑套餐*/
    //根据id查询套餐 回显套餐的基本数据
    //需要返回的参数：id语言的setmeal数据、图片的url地址
    @GetMapping("/lookdata")
    public Result lookdata(int id) {

        Setmeal setmeal = setmealService.lookdata(id);
        HashMap<String, Object> map = new HashMap<>();
        map.put("domain",QiNiuUtils.DOMAIN);
        map.put("setmeal",setmeal);
        return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, map);
    }
    /*数据回显
    根据套餐id查询套餐选中的检查组 返回id集合*/
    @GetMapping("/checkgroupIds")
    public Result findAllCkeckGroudId(int setmealid){

        List<Integer> checkgroupIds=  setmealService.findAllCheckgroudId(setmealid);
        return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkgroupIds);
    }
    /*提交修改的数据*/
    @PostMapping("/edit")
    public Result edit(@RequestBody Setmeal setmeal, Integer[] checkgroupIds ){
        setmealService.edit(checkgroupIds,setmeal);
        return new Result(true,MessageConstant.EDIT_SETMEAL_SUCCESS);
    }


}
