package com.itheima.health.Controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckgroupService;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestControllerAdvice
@RequestMapping("/checkgroup")
public class CheckgroupController {

    @Reference
    private CheckgroupService checkgroupService;


    //检查组分页查询
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){

         PageResult<CheckGroup> pageResult= checkgroupService.findPage(queryPageBean);
         return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,pageResult);

    }

    //新增检查组
    /*客户端数据：检查项的id数组 、检查组的信息
    封装数据：检查项数组： Integer[] ，检查组信息：checkGroup
    * */
    @PostMapping("/add")
    public Result add(@RequestBody CheckGroup checkGroup, Integer[] checkitemids){
        // 调用服务添加
        checkgroupService.add(checkGroup, checkitemids);
        return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }

    ///删除j检查组
    @GetMapping("/delete")
    public Result delete(int id){

        checkgroupService.deleteById(id);
        return new Result(true, MessageConstant.DELETE_CHECKGROUP_SUCCESS);
    }

    /*编辑检查组
    回显数据 检查组
    * */
    @GetMapping("/lookdata")
    public  Result lookdata(int id){
       CheckGroup checkGroup= checkgroupService.lookdata(id);

       return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroup);

    }
    /*回显数据  回显选择的检查项
    * */
    @GetMapping("/checkitemIds")
    public  Result checkitemIds(int checkgroudid){
        //获取到选中的检查项信息集合
        List<Integer> ids= checkgroupService.checkitemIds(checkgroudid);
        return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,ids);

    }


    /*提交修改*/
    @PostMapping("/update")
    public Result update(@RequestBody CheckGroup checkGroup, Integer[] checkitemIds){
        // 调用业务服务
        checkgroupService.update(checkGroup, checkitemIds);
        // 响应结果
        return new Result(true, MessageConstant.EDIT_CHECKGROUP_SUCCESS);
    }

    /*查询所有的检查项在新增套餐时使用*/
    @RequestMapping("/findAll")
    public Result findAll(){
        List<CheckGroup> checkGroups= checkgroupService.findAll();
        return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroups);

    }
}
