package com.itheima.health.Controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrdersettingService;
import com.itheima.health.util.POIUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordersetting")
public class OrdersettingController {


    @Reference
    private OrdersettingService ordersettingService;
    /**文件上传
      *点击上传excel时 ElementUI会自动发送请求到服务端
     *服务端将文件内的内容批量存入到数据库中
     * 文件中的内容有：日期+最大预约数
     * 1.使用工具类读取到excel表格里面每一行的内容
     * 2.遍历每一行的内容获取到日期 与当天的最大预约数 存入到OrderSetting中去
     * 3.获取日期  每一行第一个单元格值就是日期字符串；使用simpleDataFormat 定义格式 将读到的字符串转换成日期格式 pase()
     * 4：获取最大预约数：将字符串转换成int类型
     * 5.将OrderSetting存入集合，每一个OrderSetting就是日期与最大预约数
     * 6.调用service
    * */
    @RequestMapping("/upload")
    public Result upload(MultipartFile excelFile) throws Exception {

        /*try {*/
            //读取excel文件
            List<String[]> strings = POIUtils.readExcel(excelFile);
            SimpleDateFormat sf = new SimpleDateFormat(POIUtils.DATE_FORMAT);
            //存放结果的
            List<OrderSetting> orderSettingList = new ArrayList<>();

            //遍历集合获取里面的日期与最大预约数
            for (String[] string : strings) {
                //获取到日期字符串,同时转换成日期格式
                Date parse = sf.parse(string[0]);
                //获取最大预约数
                int i = Integer.parseInt(string[1]);

                //将数据存入到OrderSetting中 每一行都是一个 保为一个集合
                OrderSetting orderSetting = new OrderSetting(parse, i);
                orderSettingList.add(orderSetting);
            }
            //调用service完成功能额
            ordersettingService.addOrder(orderSettingList);
       /* } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "上传失败la1");

        }*/
        return new Result(true, MessageConstant.UPLOAD_SUCCESS);
    }

    //显示所有预约信息
    @RequestMapping("/getOrderSettignByMonth")
    public Result findAllOrder(String month){
        //month是年与月的拼接
// 调用服务查询
        List<Map<String,Integer>> monthData =  ordersettingService.getOrderSettignByMonth(month);
        return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS,monthData);
    }

    //设置可预约人数
    @PostMapping("/setMAXnumber")
    public Result setMAXNumber(@RequestBody OrderSetting orderSetting){

     ordersettingService.setMaxNumber(orderSetting);
     return new Result(true,MessageConstant.ORDERSETTING_SUCCESS);

    }

}
