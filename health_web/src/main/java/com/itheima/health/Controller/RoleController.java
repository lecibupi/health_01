package com.itheima.health.Controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RoleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Role")
public class RoleController {
    private static String ResultInfo = "数据成功返回";
    @Reference
    private RoleService roleService;

    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {
        return new Result(true, "数据查询成功", roleService.findPage(queryPageBean));
    }

    @GetMapping("/RoletableData")
    public Result RoletableData() {
        return new Result(true, "新增数据回显成功", roleService.RoletableData());
    }

    @PostMapping("/RoleAdd")
    public Result RoleAdd(@RequestParam(value ="menuIds")List<Integer> menuIds,@RequestParam(value ="permissionIds")List<Integer> permissionIds,@RequestBody Role formData) {
        roleService.RoleAdd(menuIds, permissionIds, formData);
        return new Result(true, "添加成功");
    }

    @GetMapping("/RoletableDataRelation")
    public Result RoletableDataRelation(Integer id) {
        return new Result(true, "数据回显", roleService.RoletableDataRelation(id));
    }

    @GetMapping("/Roledelete")
    public Result Roledelete(Integer id) {

            roleService.Roledelete(id);
            return new Result(true, "删除成功");
    }

    @PostMapping("/RoleEdit")
    public Result RoleEdit(@RequestParam(value ="menuIds")List<Integer> menuIds,@RequestParam(value ="permissionIds")List<Integer> permissionIds,@RequestBody Role formData) {

            roleService.RoleEdit(menuIds, permissionIds, formData);
            return new Result(true, "修改成功");

    }

}

