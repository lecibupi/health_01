package com.itheima.health.Controller;

import com.itheima.health.MyExecption.HealthException;
import com.itheima.health.entity.Result;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/*全局异常类 ，没有继承springMVC框架的异常处理类 使用注解@RestControllerAdvice
　在spring 3.2中，新增了@ControllerAdvice，@RestControllerAdvice 注解，
可以用于定义@ExceptionHandler、@InitBinder、@ModelAttribute，并应用到所有@RequestMapping中。
@RestControllerAdvice 是组件注解，他使得其实现类能够被classpath扫描自动发现，
如果应用是通过MVC命令空间或MVC Java编程方式配置，那么该特性默认是自动开启的。
它就是@ControllerAdvice和@ResponseBody的合并。此注解通过对异常的拦截实现的统一异常返回处理，
* */

/*这个类在controller包下已经被扫描到了可以不用再次扫描
* */

@RestControllerAdvice //被此注解修饰的类是是一个异常处理类 其作用是全局的异常处理方式
public class HealthExecption {

    //当出现自定义异常时使用此异常处理方式
    @ExceptionHandler(HealthException.class)
    public Result handleHealthException(HealthException e){
        return new Result(false, e.getMessage());
    }

    //当是最大的异常时使用此异常处理方式
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e){
        e.printStackTrace();
        return new Result(false, "操作失败，发生未知异常，请联系管理员");
    }

    /**权限不足
     * */
    @ExceptionHandler(AccessDeniedException.class)
    public Result handleAccessDeniedException(AccessDeniedException e){
        e.printStackTrace();
        return new Result(false, "权限不足");
    }

    /**用户名或者密码错误
     * */
    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public Result handleInternalAuthenticationServiceException(InternalAuthenticationServiceException e){
        e.printStackTrace();
        return new Result(false, "用户名或密码不正确");
    }

    /*
    * */
    @ExceptionHandler(BadCredentialsException.class)
    public Result handleBadCredentialsException(BadCredentialsException e){
        e.printStackTrace();
        return new Result(false, "用户名或密码不正确");
    }

}
