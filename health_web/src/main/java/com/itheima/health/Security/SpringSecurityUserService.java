package com.itheima.health.Security;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import com.itheima.health.service.securityService.SecurityServcie;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class SpringSecurityUserService implements UserDetailsService {

        @Reference
        private SecurityServcie securityServcie;

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            // 获取用户信息，包含用户下的角色集合，角色下的权限集合
            User user = securityServcie.securityFindByUsername(username);
            if(null != user){
                List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                // 加权限- 加角色，加权限
                // 获取用户的角色
                Set<Role> userRoles = user.getRoles();
                if(null != userRoles){
                    // 权限对象
                    GrantedAuthority authority = null;
                    // 遍历用户的角色集合
                    for (Role userRole : userRoles) {
                        authority = new SimpleGrantedAuthority(userRole.getKeyword());
                        // 添加角色
                        authorities.add(authority);
                        // 角色下的权限
                        if(null != userRole.getPermissions()){
                            for (Permission permission : userRole.getPermissions()) {
                                authority = new SimpleGrantedAuthority(permission.getKeyword());
                                // 添加权限
                                authorities.add(authority);
                            }
                        }
                    }
                }
                // 认证用户信息
                org.springframework.security.core.userdetails.User loginUser =
                        new org.springframework.security.core.userdetails.User(username, user.getPassword(),authorities);
                // 返回给security
                return loginUser;
            }
            // 登陆失败
            return null;
        }
    }