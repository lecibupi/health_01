package com.itheima.health.job;

import com.itheima.health.dao.OrderDao;
import com.itheima.health.dao.OrdersettingDao;
import com.itheima.health.pojo.OrderSetting;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author Robin
 * @Date 2020/8/9 0009
 */
@Component
public class ClearSettingJob {

    @Autowired
    private OrdersettingDao ordersettingDao;

    public void clearordersettings(){
    

        //获取现在时间
        Date date = new Date();
        //格式化
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(date);
        //调用dao  参数就是现在的时间
        ordersettingDao.deleteSetting(format);

    }



}
