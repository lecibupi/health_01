package com.itheima.health.job;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.SetmealDao;
import com.itheima.health.util.QiNiuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
/*图片清理过程
使用quartz框架
注册quartz：
         <!--  注册任务类  -->
         <!--  任务策略  -->
         <!--  任务触发时调用哪个bean对象 -->
         <!--  任务触发时调用的那个bean对象的方法名     -->
         <!--  任务是以多线程true还是以单线来执行false      -->
         <!--  任务触发器  -->
         <!--  绑定的任务策略      -->
         <!--  七子表达式：每间隔2秒触发1次      -->
         <!--  总调度容器  -->
         <!--  调度容器下的触发器      -->
              <!--  跳过更新检测  -->
使用七牛云工具类查询七牛云上所有的图片
调用dao查询数据库套餐里使用的图片
利用集合的差集计算七牛云中没有别使用的图片  就是需要清理的图片
* */

@Component
public class ClearImgJob {
    @Autowired
    private SetmealDao setmealDao;

    public void clearImgJob(){
        // 查七牛上的所有图片
        List<String> QiniuImg = QiNiuUtils.listFile();
        // 查数据中套餐的所有图片
        List<String> imgs = setmealDao.findImgs();
        // 七牛的减去数据库 = 需要删除的
         QiniuImg.remove(imgs);
        // 调用七牛删除, 把集合转成数组
        //先将QiniuImg集合转换为数组 此时的QiniuImg集合已经是需要清理的图片了

        String[] strings = QiniuImg.toArray(new String[]{});
        //删除
        QiNiuUtils.removeFiles(strings);
    }
}
