package com.itheima.health.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.dubbo.config.annotation.Reference;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.StringUtil;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealMobileService;
import com.itheima.health.util.QiNiuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;



import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealMobileController {
    @Reference
    private SetmealMobileService setmealMobileService;
    @Autowired
    private JedisPool jedisPool;
/*
    @Autowired
    private RedisTemplate redisTemplate;
*/

    /*查询所有的套餐
     *请求数据：无
     * 记得拼接图片路径
     * 返回参数：List<Setmeal>
     * */
    @GetMapping("/findAllSetmeal")
    public Result findAllSetmeal() {

        List<Setmeal> setmeals = null;
        Jedis jedis = jedisPool.getResource();
        String key = RedisMessageConstant.QUERYALL_SETMEAL;
        String codeInRedis = jedis.get(key);
        if (StringUtil.isEmpty(codeInRedis)) {
             setmeals = setmealMobileService.findAllSetmeal();
            /*拼接图片的路径
             * */
            for (Setmeal setmeal : setmeals) {
                setmeal.setImg(QiNiuUtils.DOMAIN + setmeal.getImg());
            }
            String json = JSONObject.toJSONString(setmeals);
            //存入redis、
            jedis.setex(key,5, json);
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeals);
        }else {
            //当redis有的缓存时 将json格式转化为setmeal格式返回客户端
            List<Setmeal> setmeal =  JSON.parseArray(codeInRedis, Setmeal.class);
            //在返回客户端
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal);
        }

    }

    /*根据套餐的id查询对应的套餐信息
     * */
    @GetMapping("/findById")
    public Result findById(int id) {
        Jedis jedis = jedisPool.getResource();
        String key = RedisMessageConstant.QUERYDATAIL_SETMEAL + "_" + id;
        String codeInRedis = jedis.get(key);

        if (StringUtil.isEmpty(codeInRedis)) {
            //为空 调用service
            Setmeal setmeal = setmealMobileService.findById(id);

            setmeal.setImg(QiNiuUtils.DOMAIN + setmeal.getImg());
            //将setmeal转化为json格式存入redis
            String json = JSONObject.toJSONString(setmeal);
            //存入redis、
            jedis.setex(key, 5,json);
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal);
        } else {
            //当redis有的缓存时 将json格式转化为setmeal格式返回客户端
            Setmeal setmeal = JSON.parseObject(codeInRedis, Setmeal.class);
            //在返回客户端
            return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal);
        }

    }

    /*根据套餐的id查询对应的套餐信息
    方式二：使用懒加载的方式操作数据库 web service里的没有变化
     * */
    @GetMapping("/findById1")
    public Result findById1(int id) {
        Setmeal setmeal = setmealMobileService.findById1(id);

        setmeal.setImg(QiNiuUtils.DOMAIN + setmeal.getImg());

        return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS, setmeal);

    }


}
