package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import com.itheima.health.util.SMSUtils;
import com.itheima.health.util.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class loginController {
@Autowired
    JedisPool jedisPool;
@Reference
MemberService memberService;

    /**登录的时候需要验证验证码是否一致 一致返回true登录成功
     *           根据电话号码查询是否是会员 不是就添加到会员
     * */
    @PostMapping("check")
    public Result login(@RequestBody Map<String,String> loginMap, HttpServletResponse response ){
        //验证码验证
        String telephone = loginMap.get("telephone");
        String code = loginMap.get("validateCode");

        //获取Redis里面的验证码
        Jedis jedis = jedisPool.getResource();
        String key = RedisMessageConstant.SENDTYPE_LOGIN + "_" + telephone;
        String jedisCode = jedis.get(key);
        //验证key的值是否与提交的一致
        if (key == null){
            return new Result(false,"验证码已过期，请重新发送");
        }
        if (!jedisCode.equals(code)){
            return new Result(false,"验证码有误");
        }
        //通过验证删除key
       // jedis.del(key);
        //判断是否为会员
     Member member=    memberService.findMember(telephone);
     if (member == null){
          member = new Member();
          member.setRegTime(new Date());
          member.setPhoneNumber(telephone);
         memberService.add(member);
     }
     //配置cookie的数据
     Cookie cookie =new Cookie("login_member_telephone",telephone);
     cookie.setMaxAge(30*24*60*60);
     cookie.setPath("/");
     response.addCookie(cookie);
     return new Result(true,MessageConstant.LOGIN_SUCCESS);

    }
}
