package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.OrderService;
import com.itheima.health.service.SetmealMobileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;
    @Autowired
    private JedisPool jedisPool;

    @Reference
    SetmealMobileService setmealMobileService;


    /**预约体检添加信息
    验证码的验证，验证2通过在发送请求
    验证验证码：
       获取请求中的验证码
      获取Redis中保存的验证码
     对比
    * */
    @PostMapping("/submit")
    public Result add(@RequestBody Map<String,Object> addMap){


        //获取Redis中的验证码
        Jedis jedis = jedisPool.getResource();
        String telephone= (String) addMap.get("telephone");
        String key = RedisMessageConstant.SENDTYPE_ORDER + "_" + telephone;
        String code = jedis.get(key);


        if (StringUtils.isEmpty(code)) {
            //为空说明验证码不存在了
            return new Result(false, "请重新获取验证码");
        }
        ///获取参数中的验证码 与Redis中的对比
        if(!code.equals(addMap.get("validateCode"))){
            //不等于说明验证码错误
            return new Result(false, "验证码有误");
        }
        //到这说明验证码一致将删除Reddis里面的验证码 已经被使用了不能在使用
       // jedis.del(key);//为了省钱不删
        //备注这个是微信预约还是电话预约
        addMap.put("orderType",Order.ORDERTYPE_WEIXIN);
        //存在调用服务
        Order order= orderService.add(addMap);
       return new Result(true, MessageConstant.ORDER_SUCCESS,order);
    }

    //预约成功以后跳转到orderSuccess.html 需要将页面数据回显
    /*返回的数据：检查人：在会员表
                体检套餐;在套餐表
                体检日期：order表
                预约类型:order表
                返回一个map
    * */
    @GetMapping("/findById")
    public  Result findByid(int id){

        Map<String,Object> map = orderService.findByid(id);
        return new Result(true,MessageConstant.ORDER_SUCCESS,map);
    }

 /*   *//**
     * 导出预约成功信息
     * 使用itext生成PDF文档
     *
     * 1.先根据订单的id查询当前订单的详情，
     * 2，在订单详情中获取套餐id 在根据套餐id查询当前套餐的检查组与检查项信息
     * 3.创建文本
     * 4.设置响应头response为文件下载
     * 5.创建一个表格，往表格里面填写订单数据
     * 6.将表格添加到pdf中去
     * 7.http://localhost:8080/pages/orderSuccess.html?orderId=28
     *//*
    @GetMapping("/exportSetmealInfo")
    public Result exportSetmealInfo(int id, HttpServletResponse res){
        // 查询预约订单信息
        Map<String, Object> orderInfo = orderService.findByid(id);
        // 获取套餐id
        int setmeal_id = (Integer)orderInfo.get("setmeal_id");
        // 查询套餐详情
        Setmeal setmeal = setmealMobileService.findById(setmeal_id);
        // 创建pdf文档 itext
        Document doc = new Document();
        // 设置pdfwriter的输出流为reponse.getoutstream
        try {
            // 设置响应头,附件下载文件
            String filename = new String("预约成功信息.pdf".getBytes(),"ISO-8859-1");
            res.setHeader("Content-Disposition","attachment;filename="+filename);
            // 内容体的类型是一个pdf文档
            res.setContentType("application/pdf");
            PdfWriter.getInstance(doc, res.getOutputStream());
            doc.open();
            // 设置表格字体
            BaseFont cn = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H",false);
            Font font = new Font(cn, 10, Font.NORMAL, Color.BLUE);
            // 填写订单信息
            doc.add(new Paragraph("预约信息",font));
            doc.add(new Paragraph("体检人:" + (String)orderInfo.get("member") ,font));
            doc.add(new Paragraph("体检套餐:" + (String)orderInfo.get("setmeal") ,font));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String orderDate = sdf.format((Date) orderInfo.get("orderDate"));
            doc.add(new Paragraph("体检日期:" + orderDate ,font));
            doc.add(new Paragraph("预约类型:" + (String)orderInfo.get("orderType") ,font));

            // 填写套餐内容
            // 套餐详情
            Table table = new Table(3); // 3列  表头
            //======================== 表格样式 ========================
            // 向document 生成pdf表格
            table.setWidth(80); // 宽度
            table.setBorder(1); // 边框
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER); //水平对齐方式
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP); // 垂直对齐方式
            *//*设置表格属性*//*
            table.setBorderColor(new Color(0, 0, 255)); //将边框的颜色设置为蓝色
            table.setPadding(5);//设置表格与字体间的间距
            //table.setSpacing(5);//设置表格上下的间距
            table.setAlignment(Element.ALIGN_CENTER);//设置字体显示居中样式

            // 表头
            table.addCell(buildCell("项目名称",font));
            table.addCell(buildCell("项目内容",font));
            table.addCell(buildCell("项目解读",font));

            // 检查组与检查项
            List<CheckGroup> checkGroups = setmeal.getCheckGroups();
            if(null != checkGroups){
                for (CheckGroup checkGroup : checkGroups) {
                    table.addCell(buildCell(checkGroup.getName(),font)); // 检查组名称
                    // 检查项
                    List<CheckItem> checkItems = checkGroup.getCheckItems();
                    StringBuilder sb = new StringBuilder();
                    if(null != checkItems){
                        for (CheckItem checkItem : checkItems) {
                            sb.append(checkItem.getName()).append(" ");
                        }
                        sb.setLength(sb.length()-1); // 去掉最后的空格
                    }
                    table.addCell(buildCell(sb.toString(),font));
                    // 检查组说明
                    table.addCell(buildCell(checkGroup.getRemark(),font));
                }
            }
            // 添加表格到文档中
            doc.add(table);
            doc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 传递内容和字体样式，生成单元格
    private Cell buildCell(String content, Font font)
            throws BadElementException {
        Phrase phrase = new Phrase(content, font);
        return new Cell(phrase);
    }*/
}
