package com.itheima.health.controller;

import com.aliyuncs.exceptions.ClientException;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.constant.RedisMessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.util.SMSUtils;
import com.itheima.health.util.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {
    @Autowired
    private JedisPool jedisPool;


    @GetMapping("/send4Order")
    public Result getValidateCode (String telephone){
        /*预约时发送验证码


          首先判断验证码是否发送过 先查Redis中
              不存在就发送 调用SMSutils工具类发送  UUID生成6位数的验证码发送 并存入Redis中设置有效期
              存在就提示一样发送了
        * */
        Jedis jedis = jedisPool.getResource();//获取连接
        //拼接key查询Redis中是否存在这个key
        String key = RedisMessageConstant.SENDTYPE_ORDER+"_"+telephone;
        //去Redis中查询
        String s = jedis.get(key);
        //判断是否存在
        if (s ==null){
            //不存在就发送验证码，存入Redis中并且设置验证码的有效时间
            Integer code = ValidateCodeUtils.generateValidateCode(6);
            String codeStr = String.valueOf(code);

            try {
                //发送
              // SMSUtils.sendShortMessage(SMSUtils.VALIDATE_CODE,telephone,codeStr);
                 jedis.set("001_15674827330","666666");
                //存入Redis 并且设置有效时间
                //jedis.setex(key,15*60,codeStr);
                //返回
                return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);

            } catch (Exception e) {
                e.printStackTrace();
                return new Result(false, MessageConstant.VALIDATECODE_ERROR);
            }

        }
        //存在
        return  new Result(true,"验证码以经发送，请注意查收");


    }
    /**
     * 发送登陆手机验证码
     */
    @PostMapping("/send4Login")
    public Result send4Login(String telephone) {
        //- 生成验证码之前要检查一下是否发送过了, 通过redis获取key为手机号码，看是否存在
        Jedis jedis = jedisPool.getResource();
        String key = RedisMessageConstant.SENDTYPE_LOGIN + "_" + telephone;
        // redis中的验证码
        String codeInRedis = jedis.get(key);
        if (null == codeInRedis) {
            //- 不存在，没发送，生成验证码，调用SMSUtils.发送验证码，把验证码存入redis(5,10,15分钟有效期)，value:验证码, key:手机号码
            Integer code = ValidateCodeUtils.generateValidateCode(6);
            try {
                // 发送验证码
                //SMSUtils.sendShortMessage(SMSUtils.VALIDATE_CODE, telephone, code + "");
                jedis.set("002_15674827330","666666");
                // 存入redis，有效时间为15分钟
                //jedis.setex(key, 15*60, code + "");
                // 返回成功
                return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
            } catch (Exception e) {
                e.printStackTrace();
                // 发送失败
                return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
            }
        }
        //- 存在：验证码已经发送了，请注意查收
        return new Result(false, "验证码已经发送了，请注意查收");
    }




}
